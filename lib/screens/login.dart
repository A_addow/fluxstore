import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';
import '../generated/i18n.dart';
import '../models/app.dart';
import '../models/user.dart';
import 'registration.dart';

class LoginScreen extends StatefulWidget {
  final bool fromCart;

  LoginScreen({this.fromCart = false});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginScreen> {
  String username, password;
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  var parentContext;
  final _auth = FirebaseAuth.instance;
  void _welcomeMessage(user, context) {
    if (widget.fromCart) {
      Navigator.of(context).pop(user);
    } else {
      final snackBar =
          SnackBar(content: Text(S.of(context).welcome + ' ${user.name} !'));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  void _failMessage(message, context) {
    /// Showing Error messageSnackBarDemo
    /// Ability so close message
    final snackBar = SnackBar(
      content: Text('Warning: $message'),
      duration: Duration(seconds: 30),
      action: SnackBarAction(
        label: S.of(context).close,
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );

    Scaffold.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    parentContext = context;
    final appConfig = Provider.of<AppModel>(context).appConfig;
    String forgetPasswordUrl = appConfig['server']['forgetPassword'];

    Future launchForgetPassworldURL(String url) async {
      if (await canLaunch(url)) {
        await launch(url, forceSafariVC: true, forceWebView: true);
      } else {
        print("Error on launching $forgetPasswordUrl");
      }
    }

    _login(context) async {
      if (username == null || password == null) {
        var snackBar = SnackBar(content: Text(S.of(context).pleaseInput));
        Scaffold.of(context).showSnackBar(snackBar);
      } else {
        Provider.of<UserModel>(context).login(
          username: username.trim(),
          password: password.trim(),
          success: (user) {
            _welcomeMessage(user, context);
            print('user name $username password $password');

            _auth
                .signInWithEmailAndPassword(email: username, password: password)
                .catchError((onError) {
              if (onError.code == 'ERROR_USER_NOT_FOUND')
                _auth
                    .createUserWithEmailAndPassword(
                        email: username, password: password)
                    .then((_) {
                  _auth.signInWithEmailAndPassword(
                      email: username, password: password);
                });
            });
          },
          fail: (message) => _failMessage(message, context),
        );
      }
    }

    _loginFacebook(context) async {
      Provider.of<UserModel>(context).loginFB(
        success: (user) => _welcomeMessage(user, context),
        fail: (message) => _failMessage(message, context),
      );
    }

    _loginSMS(context) async {
      Provider.of<UserModel>(context).loginSMS(
        success: (user) => _welcomeMessage(user, context),
        fail: (message) => _failMessage(message, context),
      );
    }

    return Scaffold(
      backgroundColor: Colors.grey[50], //Theme.of(context).backgroundColor,
      // appBar: AppBar(
      //   titleSpacing: 0.0,
      //   title: Row(
      //     mainAxisAlignment: MainAxisAlignment.start,
      //     crossAxisAlignment: CrossAxisAlignment.center,
      //     children: <Widget>[
      //       Stack(
      //         alignment: Alignment.center,
      //         children: <Widget>[
      //           IconButton(
      //             icon: Icon(
      //               FontAwesomeIcons.shoppingCart,
      //               size: 15.0,
      //             ),
      //             onPressed: () {},
      //           ),
      //           Positioned(
      //             top: 5.0,
      //             left: 5.0,
      //             width: 10.0,
      //             height: 16.0,
      //             child: Text(
      //               '0',
      //               style: TextStyle(color: Colors.grey),
      //             ),
      //           )
      //         ],
      //       ),
      //       IconButton(
      //           icon: Icon(FontAwesomeIcons.search),
      //           onPressed: () {},
      //           iconSize: 15.0),
      //       Expanded(
      //         child: Center(
      //           child: Image.asset(
      //             "assets/images/JoharhLogo.jpg",
      //             fit: BoxFit.cover,
      //             height: 40.0,
      //             width: 50.0,
      //           ),
      //         ),
      //       )
      //     ],
      //   ),
      //   automaticallyImplyLeading: false,
      //   centerTitle: true,
      //   actions: <Widget>[
      //     Row(
      //       children: <Widget>[
      //         IconButton(
      //           icon: Icon(Icons.menu),
      //           onPressed: () {},
      //         ),
      //       ],
      //     )
      //   ],
      //   backgroundColor: Colors.white,
      //   elevation: 0.0,
      // ),
      body: SafeArea(
        child: Builder(
          builder: (context) => Container(
            margin: EdgeInsets.all(
                10.0), //anaa container kuderay siyaan spacearound
            child: Stack(children: [
              ListenableProvider<UserModel>(
                create: (cntext) => UserModel(), //anaa kabedelay builder
                child: Consumer<UserModel>(builder: (context, model, child) {
                  return SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 2.0),
                    child: Column(
                      children: <Widget>[
                        // const SizedBox(height: 80.0),
                        Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Text(
                                    'تسجيل الدخول',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),

                        // const SizedBox(height: 120.0),

                        Container(
                          alignment: Alignment.topRight,
                          child: Stack(
                            children: [
                              Text('اسم المستخدم او البريد الالكتروني'),
                              Positioned(
                                child: Text(
                                  '*',
                                  style: TextStyle(
                                      fontSize: 20.0, color: Colors.red),
                                ),
                                left: 1,
                                top: 1,
                                bottom: 10,
                              )
                            ],
                          ),
                        ),

                        TextField(
                            controller: _usernameController,
                            onChanged: (value) => username = value,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                            )),

                        const SizedBox(height: 12.0),
                        //Stack(children: <Widget>[
                        Container(
                          alignment: Alignment.topRight,
                          child: Stack(
                            children: [
                              Text('كلمة المرور'),
                              Positioned(
                                child: Text(
                                  '*',
                                  style: TextStyle(
                                      fontSize: 20.0, color: Colors.red),
                                ),
                                left: 1,
                                top: 1,
                                bottom: 10,
                              )
                            ],
                          ),
                        ),

                        TextField(
                          onChanged: (value) => password = value,
                          obscureText: true,
                          controller: _passwordController,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                          ),
                        ),
                        // Positioned(
                        //   right: 4,
                        //   bottom: 20,
                        //   child: GestureDetector(
                        //     child: Text(
                        //       " " + S.of(context).reset,
                        //       style: TextStyle(
                        //           color: Theme.of(context).primaryColor),
                        //     ),
                        //     onTap: () {
                        //       launchForgetPassworldURL(forgetPasswordUrl);
                        //     },
                        //   ),
                        // )
                        // ]),
                        SizedBox(
                          height: 60.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text('تذكرني'),
                                Checkbox(
                                  value: true,
                                  onChanged: null,
                                )
                              ],
                            ),
                                InkWell(
                              child: Text('نسيت كلمة المرور؟'),
                              onTap: () {
                                launchForgetPassworldURL(forgetPasswordUrl);
                              },
                              enableFeedback: true,
                            ),
                          ],
                        ),
                        MaterialButton(
                          onPressed: () {
                            _login(context);
                          },
                          minWidth: 300.0,
                          child: Text('تسجيل الدخول'),
                          color: Colors.black,
                          textColor: Colors.white,
                        ),
                        //   SignInButtonBuilder(
                        //   text: 'تسجيل الدخول',
                        //   icon: Icons.email,
                        //   onPressed: () {
                        //     _login(context);
                        //   },
                        //   padding: EdgeInsets.only(top: 10, bottom: 10),
                        //   elevation: 0,
                        //   backgroundColor: Theme.of(context).primaryColor,
                        // ),
                        SizedBox(
                          height: 10.0,
                        ),

                        Stack(
                            alignment: AlignmentDirectional.center,
                            children: <Widget>[
                              SizedBox(
                                  height: 60.0,
                                  width: 200.0,
                                  child: Divider(color: Colors.grey.shade300)),
                              Container(
                                  height: 30,
                                  width: 40,
                                  color: Colors.grey[50]),
                              Text("أو",
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey.shade400))
                            ]),
                            MaterialButton(
                              onPressed: () =>
                                  Navigator.pushNamed(context, '/register'),
                              minWidth: 300.0,
                              child: Text('انشاء حساب جديد'),
                              color: Colors.white,
                            ),
                        Row(
                          mainAxisSize:MainAxisSize.max ,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            IconButton(
                              onPressed: () => _loginFacebook(context),
                              icon: Icon(
                                FontAwesomeIcons.facebookF,
                                color: Color(0xFF4267B2),
                                size: 10.0,
                              ),
                            ),
                            IconButton(
                              onPressed: () => _loginSMS(context),
                              icon: Icon(
                                FontAwesomeIcons.instagram,
                                color: Colors.lightBlue,
                                size: 15.0,
                              ),
                              
                            ),
                            IconButton(
                              onPressed: () => _loginSMS(context),
                              icon: Icon(
                                FontAwesomeIcons.snapchat,
                                color: Colors.lightBlue,
                                size: 15.0,
                              ),
                             
                            ),
                            IconButton(    //RawMaterialButton
                              onPressed: () => _loginSMS(context),
                              icon: Icon(
                                FontAwesomeIcons.twitter,
                                color: Colors.lightBlue,
                                size: 15.0,
                              ),
                              // shape: CircleBorder(),
                              // elevation: 0.4,
                              // fillColor: Colors.grey.shade50,
                              // padding: const EdgeInsets.all(5.0),
                            ),
                          ],
                        ),

                        SizedBox(
                          height: 30.0,
                        ),
                        // Column(
                        //   children: <Widget>[
                        //     Row(
                        //       mainAxisAlignment: MainAxisAlignment.center,
                        //       children: <Widget>[
                        //         Text(S.of(context).dontHaveAccount),
                        //         GestureDetector(
                        //           onTap: () {
                        //             Navigator.push(
                        //               context,
                        //               MaterialPageRoute(
                        //                 builder: (BuildContext context) =>
                        //                     RegistrationScreen(),
                        //               ),
                        //             );
                        //           },
                        //           child: Text(" " + S.of(context).signup,
                        //               style: TextStyle(
                        //                   fontWeight: FontWeight.bold,
                        //                   color: Theme.of(context).primaryColor)),
                        //         ),
                        //       ],
                        //     ),
                        //   ],
                        // )

//            FlatButton(
//              child: const Text('CANCEL'),
//              shape: const BeveledRectangleBorder(
//                borderRadius: BorderRadius.all(Radius.circular(7.0)),
//              ),
//              onPressed: () {
//                _usernameController.clear();
//                _passwordController.clear();
//                Navigator.pop(context);
//              },
//            ),
                      ],
                    ),
                  );
                }),
              ),
              if (widget.fromCart)
                Positioned(
                    left: 10,
                    child: IconButton(
                        icon: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        })),
            ]),
          ),
        ),
      ),
    );
  }
}

class PrimaryColorOverride extends StatelessWidget {
  const PrimaryColorOverride({Key key, this.color, this.child})
      : super(key: key);

  final Color color;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Theme(
      child: child,
      data: Theme.of(context).copyWith(primaryColor: color),
    );
  }
}
